use std::io::{BufRead, BufReader};
use std::os::unix::net::{UnixListener, UnixStream};
use std::process::Command;
use std::{thread, time};
use std::time::Instant;

use nix::sys::socket::socket;
use nix::sys::socket::AddressFamily;
use nix::sys::socket::SockFlag;
use nix::sys::socket::SockType;
use nix::sys::socket::UnixAddr;
use std::sync::mpsc::channel;

fn handle_client(stream: UnixStream) {
    let stream = BufReader::new(stream);
    for line in stream.lines() {
        println!("{}", line.unwrap());
    }
}

fn main() {
    let now = Instant::now();

    let (sender, receiver) = channel::<()>();
    
    let path = "rust-uds";
    // let path = String::from("nix\0abstract\0test");

    let raw_fd = socket(
        AddressFamily::Unix,
        SockType::Stream,
        SockFlag::SOCK_CLOEXEC,
        None,
    )
    .unwrap();

    let addr = UnixAddr::new_abstract(path.as_bytes()).unwrap();
    // let sun_path1 = addr.sun_path();
    // println!("sun_path1: {:?}", sun_path1);

    nix::sys::socket::bind(raw_fd, &nix::sys::socket::SockAddr::Unix(addr)).unwrap();

    // let listener = UnixListener::bind(path).unwrap();

    thread::spawn(move || {
        // for stream in listener.incoming() {
        // println!("listen");
        nix::sys::socket::listen(raw_fd, 1).unwrap();
        // println!("listen OK");

        match nix::sys::socket::accept(raw_fd) {
            Ok(fd) => {
                use nix::unistd::{read, write};
                // println!("accepted");

                loop {
                    // println!("wait");
                    let mut data = vec![0u8; 1024];
                    let num_bytes = read(fd, &mut data).expect("read encrypt");
                    // println!("{:?}", data);

                    // let str = String::from_utf8_lossy(&data[..num_bytes]);
                    // println!("> {:?}", str);

                    // println!("{}", now.elapsed().as_secs());
                    // println!("{}", now.elapsed().subsec_millis());

                    drop(sender);

                    
                    break;
                }
            }
            Err(err) => {
                println!("Error: {}", err);
            } /*
              for stream in nix::sys::socket::accept(raw_fd) {
                  match stream {
                      Ok(stream) => {
                          thread::spawn(|| handle_client(stream));
                      }
                      Err(err) => {
                          println!("Error: {}", err);
                          break;
                      }
                  }*/
        }
    });

    // println!("path: {:?}", path);

    let out = Command::new("am")
        .args(&[
            "broadcast",
            "--user",
            "0",
            "-n",
            "com.termux.api/.TermuxApiReceiver",
            "--es",
            "socket_output",
            path,
            "--es",
            "api_method",
            "BatteryStatus",
        ])
        .output()
        .expect("failed to execute process");

    // println!("out: {:?}", out);

    receiver.recv();
}
